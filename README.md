# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

JS ERROR LOGGER FRAMEWORK

Collect and send Javascript dumps to your SAP Backend

### How do I get set up? ###

SAMPLE IMPLEMENTATION IN COMPONENT.JS

	//init error logger
	this.oErrorLogger = ErrorLogger.getInstance({
		sServiceUrl: "/sap/opu/odata/sap/ZJS_ERROR_LOGGER_SRV",
		sErrorEntity: "Session"
	});

