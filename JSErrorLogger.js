sap.ui.define([
	"sap/ui/base/Object",
	"sap/ui/model/odata/ODataModel"
], function(Object, ODataModel) {
	"use strict";

	var constDeviceTypeDesktop = "DESKTOP",
		constDeviceTypeTablet = "TABLET",
		constDeviceTypePhone = "PHONE";

	var instance;

	var JSErrorLogger = Object.extend("JSErrorLogger", {
		constructor: function(oParameters) {

			var oSessionDateUTC = null;
			var sSessionTimeUTC = "";
			var sDevice = "";

			this._sServiceUrl = oParameters.sServiceUrl;
			this._sErrorEntity = oParameters.sErrorEntity;
			this._oEventBus = sap.ui.getCore().getEventBus();
			this._bPushedManual = false;

			oSessionDateUTC = this._dateToUTC(new Date());
			sSessionTimeUTC = this._dateToEdmTime(new Date());

			if (sap.ui.Device.system.desktop) {
				sDevice = constDeviceTypeDesktop;
			} else if (sap.ui.Device.system.phone) {
				sDevice = constDeviceTypePhone;
			} else if (sap.ui.Device.system.tablet) {
				sDevice = constDeviceTypeTablet;
			} else {
				sDevice = "UNKNOWN";
			}

			this._oErrorObject = {
				SessionId: "",
				Timestamp: Date.now().toString(),
				SessionDate: oSessionDateUTC,
				SessionTime: sSessionTimeUTC,
				Device: sDevice,
				Errors: []
			};

			this._oODataModel = new ODataModel(oParameters.sServiceUrl, {
				json: true
			});

			this._setErrorHandling();

		},

		getSessionInfo: function() {
			var oErrorObject = JSON.parse(JSON.stringify(this._oErrorObject));

			delete oErrorObject.Errors;

			oErrorObject.SessionDate = new Date(oErrorObject.SessionDate);
			oErrorObject.SessionTime = new Date(oErrorObject.SessionDate);

			return oErrorObject;
		},

		pushErrorsToBackend: function() {
			this._bPushedManual = true;
			this._sendErrorsToBackend();
		},

		kill: function() {
			instance = undefined;
		},

		_setErrorHandling: function() {

			var that = this,
				bSend = false;

			$(window).bind("beforeunload", function(e) {
				if (that._bPushedManual && that._oErrorObject.Errors.length === 0) {
					bSend = false;
				} else {
					bSend = true;
				}

				if (bSend) {
					that._sendErrorsToBackend();
				}
			});

			window.onerror = function(message, url, lineNumber) {
				that._addNewError(message, url, lineNumber);
				return true;
			};
		},

		_addNewError: function(sMessage, sUrl, sLineNumber) {

			var sErrorLocation = "",
				aErrorLocationSplit = null,
				oError = null,
				oErrorDateUTC = null,
				sErrorTimeUTC = "";

			aErrorLocationSplit = sUrl.split("/");
			sErrorLocation = aErrorLocationSplit[aErrorLocationSplit.length - 1].split("?")[0];
			oErrorDateUTC = this._dateToUTC(new Date());
			sErrorTimeUTC = this._dateToEdmTime(oErrorDateUTC);

			oError = {
				SessionId: "",
				Timestamp: Date.now().toString(),
				ErrorDate: oErrorDateUTC,
				ErrorTime: sErrorTimeUTC,
				Message: sMessage,
				Line: sLineNumber,
				Location: sErrorLocation
			};

			this._publishErrorEvent(oError);
			this._oErrorObject.Errors.push(oError);

		},

		_publishErrorEvent: function(oError) {
			this._oEventBus.publish("ErrorLogger", "NewError", oError);
		},

		_publishSendEvent: function(oData) {
			this._oEventBus.publish("ErrorLogger", "Send", oData);
		},

		_sendErrorsToBackend: function() {

			var that = this,
				sPath = "/" + this._sErrorEntity + "Set";

			this._oODataModel.create(sPath, this._oErrorObject, {
				async: false,
				success: function(oData) {
					that._oErrorObject.SessionId = oData.SessionId;
				},
				error: function() {}
			});

			this._oErrorObject.Errors = [];

			this._publishSendEvent(this._oErrorObject);

		},

		_dateToUTC: function(oDate) {
			return new Date(Date.UTC(oDate.getFullYear(), oDate.getMonth(), oDate.getDate(),
				oDate.getHours(), oDate.getMinutes(), oDate.getSeconds()));
		},

		_dateToEdmTime: function(oDate) {
			var oFormatter = sap.ui.core.format.DateFormat.getDateInstance({
				pattern: "PTkk'H'mm'M'ss'S'"
			});

			return oFormatter.format(new Date());
		}

	});

	return {
		getInstance: function(mParameters) {
			if (!instance) {
				instance = new JSErrorLogger(mParameters);
			}
			return instance;
		}
	};
});